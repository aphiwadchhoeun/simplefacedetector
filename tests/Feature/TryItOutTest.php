<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TryItOutTest extends TestCase
{

    /**
     * Test Crash
     *
     * @return void
     */
    public function testCrash()
    {
        $response = $this->get(route('sfd.try-it-out'));
        $response->assertStatus(200);
    }


    /**
     * Test if see the title
     */
    public function testSeeTitle()
    {
        $response = $this->get(route('sfd.try-it-out'));
        $response->assertSee(trans('try-it-out/common.title'));
    }
}
