@extends('layout/default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <router-view>
                    {{--placeholder to show while Vue is loading on page load first time--}}
                    <p class="text-center" style="padding: 2em;">
                        <span class="glyphicon glyphicon-refresh spin"></span> Loading...
                    </p>
                </router-view>
            </div>
        </div>
    </div>
@endsection