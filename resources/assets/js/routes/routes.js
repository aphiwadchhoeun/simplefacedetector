/**
 * Created by Aphiwad on 4/26/2017.
 */
import Monica from '../components/page/Monica.vue';

export default [
    {
        path: '/monica',
        name: 'MonicaPage',
        component: Monica
    }
]